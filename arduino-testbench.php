<?
/*
Plugin Name: Arduino Testbench
Plugin URI: https://gitlab.com/quantr/toolchain/arduino-testbench
Description: Arduino testbench for our cpu project
Version: 1.0.0
Author: Peter
Support URI: https://gitlab.com/quantr/toolchain/arduino-testbench
Author URI: https://gitlab.com/quantr/toolchain/arduino-testbench
License: Quantr Open Source License
*/

// require_once('vendor/autoload.php');
include(ABSPATH . '/wp-includes/pluggable.php');
include(plugin_dir_path(__FILE__) . 'public/shortcode.php');
// include(plugin_dir_path(__FILE__) . 'admin/menu.php');


require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
$charset_collate = $wpdb->get_charset_collate();

define('ARDUINO_TESTBENCH_TABLE_PREFIX', 'arduino_testbench_');

define('at_log', ARDUINO_TESTBENCH_TABLE_PREFIX.'log');
$sql = "CREATE TABLE " . at_log . " (
	id int(9) NOT NULL AUTO_INCREMENT,
	date datetime NULL,
	message text NOT NULL,
	PRIMARY KEY (id)
) $charset_collate;";
dbDelta($sql);

function logFunc(WP_REST_Request $request)
{
	global $wpdb;
	// https://www.quantr.foundation/wp-json/arduino-testbench/log
	$wpdb->insert(
		at_log,
		array(
			'message' => $request['message'],
			'date' => current_time('mysql')
		)
	);
	echo "ok";
}

add_action('rest_api_init', function () {
	register_rest_route(get_plugin_data(__FILE__)['TextDomain'], '/log', array(
		'methods' => 'POST',
		'callback' => 'logFunc',
	));
});
