<?php
require $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
global $wpdb;

if (is_user_logged_in()) {
	$user = [
		'name' => wp_get_current_user()->user_name,
		'email' => wp_get_current_user()->user_email
	];
} else {
	$user = [];
}

// if (wp_get_current_user()->user_email == 'peter@quantr.hk') {
// 	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . QUANTR_DOCS_TABLE_PREFIX . "project order by name;", null));
// } else {
// 	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . QUANTR_DOCS_TABLE_PREFIX . "project where debug=0 order by name;", null));
// }
?>

<!-- <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__) ?>/app/assets/css/app.css" type="text/css"> -->
<script type="text/javascript">
	// var STATIC_URL = 'http://localhost/my-app';
	var userCredential = <?= json_encode($user); ?>;
	var plugin_dir_url = "<?= plugin_dir_url(__FILE__) ?>";
	var myConfig = {
		homeUrl		:	"https://www.quantr.foundation/arduino-testbench/",
	};
	peterfuckingpath = `<?= plugin_dir_url(__FILE__) ?>/app/assets/bundle/`;
</script>
<div id="app"></div>
<script type="text/javascript" src="<?= plugin_dir_url(__FILE__) ?>/library/particlejs/particles.min.js"></script>
<script type="text/javascript" src="<?= plugin_dir_url(__FILE__) ?>/app/assets/bundle/main.bundle.js"></script>

