import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './Main.module.scss';
import Header from './Header.jsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as fontawesome from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery';

const axios = require('axios');
export default class Main extends Component {
	rows = [];

	constructor() {
		super();
		let that = this;
		axios.get(plugin_dir_url + '/api.php?action=getLogs').then(function (response) {
			let json = response.data;
			that.rows = [];
			for (let x = 0; x < json.length; x++) {
				let item = json[x];
				that.rows.push(
					<tr>
						<td>{item['id']}</td>
						<td>{item['date']}</td>
						<td>{item['message']}</td>
					</tr>
				);
			}
			that.forceUpdate();
		});
	}

	render() {
		return (
			<div className="main">
				{/* <Header type="main" /> */}
				<div style={{ textAlign: 'center' }}>
					<span style={{ padding: '10px', textAlign: 'center', color: '#a17fc9', border: '2px solid #a17fc9', borderRadius: '5px' }}>POST to https://www.quantr.foundation/wp-json/arduino-testbench/log with param 'message'</span>
				</div>
				<table class="table table-striped" style={{ marginTop: '20px' }}>
					<thead>
						<tr>
							<th>ID</th>
							<th>Date</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody>
						{this.rows}
					</tbody>
				</table>
			</div>
		)
	}
}
