<?
function arduino_testbench_init()
{
    function arduino_testbench_shortcode($atts = [], $content = null)
    {
		ob_start();
        include 'html.php';
        $content = ob_get_clean();
        return $content;
    }
    add_shortcode('arduino_testbench', 'arduino_testbench_shortcode');
}
add_action('init', 'arduino_testbench_init');
